package Practica17;

public class Operations {

    public boolean rangeNumberVerify(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean typeVariableVerify(String numbers) {
        for (int i = 0; i < numbers.length(); i++) {
            if (numbers.charAt(i) == '.' || numbers.charAt(i) == ',')
                return false;
        }
        return true;
    }

    public boolean parenthesisVerify(String parenthesis) {
        int firstParenthesis = 0;
        int lastParenthesis = 0;
        char character, characterBefore, characterNext;
        for (int i = 0; i < parenthesis.length(); i++) {
            character = parenthesis.charAt(i);
            if (i == parenthesis.length()) characterNext = parenthesis.charAt(i + 1);
            else characterNext = ' ';
            if (character == '(' && characterNext != ')') return true;
            if (i > 0) characterBefore = parenthesis.charAt(i - 1);
            else characterBefore = ' ';
            if (character == ')' && characterBefore != '(') return true;
            if (character == '(') firstParenthesis++;
            if (character == ')') lastParenthesis++;
        }
        if (parenthesis.charAt(0) == ')') return false;
        if (firstParenthesis == lastParenthesis) return true;
        else return false;
    }

    public boolean operationNotPermittedVerify(String symbol){
        for (int i = 0; i < symbol.length(); i++) {
            if (symbol.charAt(i) == '/' || symbol.charAt(i) == '-') {
                i = symbol.length();
                return true;
            }
        }
        return false;
    }

    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }

    public boolean verifyOperator(String sign){
        char element;
        for (int i = 0; i < sign.length(); i++){
            element = sign.charAt(i);
            if (element == '+' || element == '*' || element == '^' || element == '!'){
                return true;
            }
        }
        return false;
    }

    public boolean verifyNotLetters(String operation){
        for (int i = 0; i < operation.length(); i++) {
            if ((operation.charAt(i) >= 'a' && operation.charAt(i) <= 'z') || (operation.charAt(i) >= 'A' && operation.charAt(i) <= 'Z')) {
                i = operation.length();
                return true;
            }
        }
        return false;
    }

}
