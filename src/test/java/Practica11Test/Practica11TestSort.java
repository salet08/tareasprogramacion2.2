package Practica11Test;

import Practica11.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Practica11TestSort {

    @Test
    public void testSelectionSort(){
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(1);
        bag.add(2);
        bag.add(3);
        bag.add(4);
        bag.add(5);
        assertEquals("(5)root={data=5, sig->{data=4, sig->{data=3, sig->{data=2, sig->{data=1, sig->null}}}}}", bag.toString());
        bag.selectionSort();
        assertEquals("(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->null}}}}}", bag.toString());
        bag.add(8);
        bag.add(6);
        bag.add(9);
        assertEquals("(8)root={data=9, sig->{data=6, sig->{data=8, sig->{data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->null}}}}}}}}", bag.toString());
        bag.selectionSort();
        assertEquals("(8)root={data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->{data=6, sig->{data=8, sig->{data=9, sig->null}}}}}}}}", bag.toString());
    }
    @Test
    public void testBubbleSort(){
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(10);
        bag.add(50);
        bag.add(106);
        bag.add(42);
        assertEquals("(4)root={data=42, sig->{data=106, sig->{data=50, sig->{data=10, sig->null}}}}", bag.toString());
        bag.bubbleSort();
        assertEquals("(4)root={data=10, sig->{data=42, sig->{data=50, sig->{data=106, sig->null}}}}", bag.toString());
        bag.add(27);
        bag.add(109);
        bag.add(1);
        assertEquals("(7)root={data=1, sig->{data=109, sig->{data=27, sig->{data=10, sig->{data=42, sig->{data=50, sig->{data=106, sig->null}}}}}}}", bag.toString());
        bag.bubbleSort();
        assertEquals("(7)root={data=1, sig->{data=10, sig->{data=27, sig->{data=42, sig->{data=50, sig->{data=106, sig->{data=109, sig->null}}}}}}}", bag.toString());
    }
}
