package Practica4;
public class Solution {

    public static int sumGroups(int[] arr){
        int arrayLength = arr.length;
        do {
            int number = 0;
            int arrayNumber = arr[0];
            for (int i = 1; i < arrayLength; i++)
                if ((arr[i-1] + arr[i]) % 2 == 0) {
                    arrayNumber += arr[i];
                }else {
                    arr[number++] = arrayNumber;
                    arrayNumber = arr[i];
                }
            arr[number++] = arrayNumber;
            if (arrayLength == number) {
                return arrayLength;
            }
            arrayLength = number;
        } while (true);
    }
}