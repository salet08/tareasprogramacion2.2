package Practica17Test;

import Practica17.Calculator;
import Practica17.Operations;
import org.junit.Test;

import static org.junit.Assert.*;

public class Praactica17TestOperadores {

    @Test
    public void validatorBrackets() {
        Operations operations = new Operations();
        operations.parenthesisVerify("()");
        assertTrue(operations.parenthesisVerify("("));
        assertTrue(operations.parenthesisVerify(")"));
        operations.parenthesisVerify("(())");
        assertTrue(operations.parenthesisVerify("("));
        assertTrue(operations.parenthesisVerify("("));
        assertTrue(operations.parenthesisVerify(")"));
        assertTrue(operations.parenthesisVerify(")"));
    }

    @Test
    public void validatorOperationNotPemitted() {
        Operations operations = new Operations();
        assertTrue(operations.operationNotPermittedVerify("-"));
        assertTrue(operations.operationNotPermittedVerify("/"));
        assertFalse(operations.operationNotPermittedVerify("+"));
        assertFalse(operations.operationNotPermittedVerify("*"));
        assertFalse(operations.operationNotPermittedVerify("!"));
        assertFalse(operations.operationNotPermittedVerify("%"));
        assertFalse(operations.operationNotPermittedVerify(""));
    }

    @Test
    public void validatorRange() {
        Operations operations = new Operations();
        assertTrue(operations.rangeNumberVerify("2123523"));
        assertTrue(operations.rangeNumberVerify("5333"));
        assertFalse(operations.rangeNumberVerify("434351236553421314"));
        assertFalse(operations.rangeNumberVerify("1324453326552"));
        assertTrue(operations.rangeNumberVerify("3"));
        assertTrue(operations.rangeNumberVerify("343"));
        assertTrue(operations.rangeNumberVerify("9846"));
        assertTrue(operations.rangeNumberVerify("23547"));
        assertTrue(operations.rangeNumberVerify("226533"));
        assertFalse(operations.rangeNumberVerify("98763212343565432456"));
        assertFalse(operations.rangeNumberVerify("234325467098713"));
        assertFalse(operations.rangeNumberVerify("97634561253"));
        assertTrue(operations.rangeNumberVerify("673"));
        assertTrue(operations.rangeNumberVerify("95395"));
    }

    @Test
    public void validatorTypeVariable() {
        Operations operations = new Operations();
        assertFalse(operations.typeVariableVerify("."));
        assertFalse(operations.typeVariableVerify(","));
        assertTrue(operations.typeVariableVerify(":"));
        assertTrue(operations.typeVariableVerify(";"));
        assertTrue(operations.typeVariableVerify("'"));

    }

    @Test
    public void validatorOperators() {
        Operations operations = new Operations();
        assertTrue(operations.verifyOperator("1+3+4+5"));
        assertTrue(operations.verifyOperator("1578876*3"));
        assertTrue(operations.verifyOperator("23334!"));
        assertTrue(operations.verifyOperator("((2+3)!*(4*3))"));
        assertTrue(operations.verifyOperator("1325478+1"));
        assertTrue(operations.verifyOperator("9^22"));
        assertTrue(operations.verifyOperator("43*12"));
        assertTrue(operations.verifyOperator("54634+24"));
        assertTrue(operations.verifyOperator("436+43"));
        assertFalse(operations.verifyOperator("76424-34543-3253"));
        assertFalse(operations.verifyOperator("5436413/5464"));
        assertFalse(operations.verifyOperator("3423-4654"));
        assertFalse(operations.verifyOperator("65-(435/4354)"));
        assertFalse(operations.verifyOperator("(1231-645)/5453"));
    }

    @Test
    public void verifyAdictionTest() {
        Calculator calculator = new Calculator("");

        assertEquals(2, calculator.operationSum("1", "1"));
        assertEquals(7482016, calculator.operationSum("6561", "7475455"));
        assertEquals(100343, calculator.operationSum("49236", "51107"));
        assertEquals(100, calculator.operationSum("90", "10"));
        assertEquals(79101259, calculator.operationSum("324365", "78776894"));
        assertEquals(24508951, calculator.operationSum("24498952", "9999"));
        assertEquals(1241, calculator.operationSum("42", "1199"));
        assertEquals(9999, calculator.operationSum("0", "9999"));
        assertEquals(11163, calculator.operationSum("52", "11111"));
        assertEquals(569124, calculator.operationSum("1234", "567890"));
        assertEquals(248165, calculator.operationSum("1357", "246808"));
    }

    @Test
    public void potenciaTest() {
        Calculator calculator = new Calculator("");

        assertEquals(4, calculator.operationPow("2^2"));
        assertEquals(7776, calculator.operationPow("6^5"));
        assertEquals(279936, calculator.operationPow("6^7"));
        assertEquals(6561, calculator.operationPow("9^4"));
        assertEquals(81, calculator.operationPow("3^4"));
        assertEquals(512, calculator.operationPow("2^9"));
        assertEquals(216, calculator.operationPow("6^3"));
        assertEquals(262144, calculator.operationPow("4^9"));
        assertEquals(6, calculator.operationPow("6^1"));
        assertEquals(32, calculator.operationPow("2^5"));
        assertEquals(6561, calculator.operationPow("3^8"));

    }

    @Test
    public void multiplicacionTest() {
        Calculator calculator = new Calculator("");

        assertEquals(4, calculator.operationMultiplication("2", "2"));
        assertEquals(408, calculator.operationMultiplication("34", "12"));
        assertEquals(34985095, calculator.operationMultiplication("6437", "5435"));
        assertEquals(1, calculator.operationMultiplication("1", "1"));
        assertEquals(1629, calculator.operationMultiplication("3", "543"));
        assertEquals(1898456, calculator.operationMultiplication("29", "65464"));
        assertEquals(5588271, calculator.operationMultiplication("64233", "87"));
        assertEquals(488595778, calculator.operationMultiplication("49423", "9886"));
        assertEquals(30216255, calculator.operationMultiplication("6043251", "5"));
        assertEquals(18539775, calculator.operationMultiplication("24235", "765"));
        assertEquals(290946096, calculator.operationMultiplication("30128", "9657"));
    }

    @Test
    public void factorialTest() {
        Calculator calculator = new Calculator("");

        assertEquals(2, calculator.operationFactorial("2"));
        assertEquals(720, calculator.operationFactorial("6"));
        assertEquals(3628800, calculator.operationFactorial("10"));
        assertEquals(6, calculator.operationFactorial("3"));
        assertEquals(24, calculator.operationFactorial("4"));
        assertEquals(5040, calculator.operationFactorial("7"));
        assertEquals(479001600, calculator.operationFactorial("12"));
        assertEquals(120, calculator.operationFactorial("5"));
        assertEquals(1, calculator.operationFactorial("1"));
        assertEquals(362880, calculator.operationFactorial("9"));
        assertEquals(40320, calculator.operationFactorial("8"));
        assertEquals(39916800, calculator.operationFactorial("11"));
    }

    @Test
    public void validatorNotLetters() {
        Operations operations = new Operations();
        assertTrue(operations.verifyNotLetters("a"));
        assertTrue(operations.verifyNotLetters("b"));
        assertTrue(operations.verifyNotLetters("c"));
        assertTrue(operations.verifyNotLetters("d"));
        assertTrue(operations.verifyNotLetters("e"));
        assertTrue(operations.verifyNotLetters("f"));
        assertTrue(operations.verifyNotLetters("g"));
        assertTrue(operations.verifyNotLetters("h"));
        assertTrue(operations.verifyNotLetters("i"));
        assertTrue(operations.verifyNotLetters("j"));
        assertTrue(operations.verifyNotLetters("k"));
        assertTrue(operations.verifyNotLetters("l"));
        assertTrue(operations.verifyNotLetters("m"));
        assertTrue(operations.verifyNotLetters("n"));
        assertTrue(operations.verifyNotLetters("o"));
        assertTrue(operations.verifyNotLetters("p"));
        assertTrue(operations.verifyNotLetters("q"));
        assertTrue(operations.verifyNotLetters("r"));
        assertTrue(operations.verifyNotLetters("s"));
        assertTrue(operations.verifyNotLetters("t"));
        assertTrue(operations.verifyNotLetters("u"));
        assertTrue(operations.verifyNotLetters("v"));
        assertTrue(operations.verifyNotLetters("w"));
        assertTrue(operations.verifyNotLetters("x"));
        assertTrue(operations.verifyNotLetters("y"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("z"));
        assertTrue(operations.verifyNotLetters("A"));
        assertTrue(operations.verifyNotLetters("B"));
        assertTrue(operations.verifyNotLetters("C"));
        assertTrue(operations.verifyNotLetters("D"));
        assertTrue(operations.verifyNotLetters("E"));
        assertTrue(operations.verifyNotLetters("F"));
        assertTrue(operations.verifyNotLetters("G"));
        assertTrue(operations.verifyNotLetters("H"));
        assertTrue(operations.verifyNotLetters("I"));
        assertTrue(operations.verifyNotLetters("J"));
        assertTrue(operations.verifyNotLetters("K"));
        assertTrue(operations.verifyNotLetters("L"));
        assertTrue(operations.verifyNotLetters("M"));
        assertTrue(operations.verifyNotLetters("N"));
        assertTrue(operations.verifyNotLetters("O"));
        assertTrue(operations.verifyNotLetters("P"));
        assertTrue(operations.verifyNotLetters("Q"));
        assertTrue(operations.verifyNotLetters("R"));
        assertTrue(operations.verifyNotLetters("S"));
        assertTrue(operations.verifyNotLetters("T"));
        assertTrue(operations.verifyNotLetters("U"));
        assertTrue(operations.verifyNotLetters("V"));
        assertTrue(operations.verifyNotLetters("W"));
        assertTrue(operations.verifyNotLetters("X"));
        assertTrue(operations.verifyNotLetters("Y"));
        assertTrue(operations.verifyNotLetters("Z"));
        assertFalse(operations.verifyNotLetters("*"));
        assertFalse(operations.verifyNotLetters("-"));
        assertFalse(operations.verifyNotLetters("+"));
        assertFalse(operations.verifyNotLetters("?"));
        assertFalse(operations.verifyNotLetters("¿"));
        assertFalse(operations.verifyNotLetters("!"));
        assertFalse(operations.verifyNotLetters(":"));
    }
}
