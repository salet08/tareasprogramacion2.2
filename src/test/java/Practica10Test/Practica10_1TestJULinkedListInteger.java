package Practica10Test;

import Practica10.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Practica10_1TestJULinkedListInteger {

    @Test
    public void testSize() {
        List<Integer> juLinkedList = new JULinkedList<>();
        assertEquals(0, juLinkedList.size());
        juLinkedList.add(2);
        juLinkedList.add(55);
        assertEquals(2, juLinkedList.size());
        juLinkedList.add(5);
        juLinkedList.add(0);
        juLinkedList.add(2);
        juLinkedList.add(1);
        juLinkedList.add(23);
        assertEquals(7, juLinkedList.size());
    }

    @Test
    public void testisEmpty(){
        List<Integer> juLinkedList = new JULinkedList<>();
        assertTrue(juLinkedList.isEmpty());
        juLinkedList.add(40);
        juLinkedList.add(29);
        juLinkedList.add(45);
        juLinkedList.add(100);
        juLinkedList.add(1);
        assertFalse(juLinkedList.isEmpty());
    }

    @Test
    public void testIterator(){
        List<Integer> juLinkedList = new JULinkedList<>();
        int counter = 0;
        while (juLinkedList.iterator().hasNext()){
            assertEquals(juLinkedList.get(counter), juLinkedList.iterator().next());
            break;
        }
        assertEquals(0, counter);
    }

    @Test
    public void testAdd() {
        List<Integer> juLinkedList = new JULinkedList<>();
        assertTrue(juLinkedList.add(1));
        assertEquals(1, juLinkedList.size());
        assertTrue(juLinkedList.add(2));
        assertTrue(juLinkedList.add(3));
        assertEquals(3, juLinkedList.size());
        assertTrue(juLinkedList.add(4));
        assertTrue(juLinkedList.add(5));
        assertTrue(juLinkedList.add(6));
        assertTrue(juLinkedList.add(7));
        assertTrue(juLinkedList.add(8));
        assertEquals(8, juLinkedList.size());
        assertTrue(juLinkedList.add(9));
        assertEquals(9, juLinkedList.size());

        Integer integer = 6;
        Integer integer1 = 2;
        Integer integer2 = 1;
        Integer integer3 = 2;
        juLinkedList.add(integer);
        juLinkedList.add(integer1);
        juLinkedList.add(integer2);
        juLinkedList.add(integer3);
        assertTrue(juLinkedList.add(integer));
        assertTrue(juLinkedList.add(integer1));
        assertTrue(juLinkedList.add(integer2));
        assertTrue(juLinkedList.add(integer3));
    }

    @Test
    public void testRemoveObject(){
        List<Integer> juLinkedList = new JULinkedList<>();
        juLinkedList.add(40);
        juLinkedList.add(29);
        juLinkedList.add(45);
        juLinkedList.add(100);
        juLinkedList.add(1);
        Object add1 = 40;
        Object add2 = 29;
        Object add3 = 45;
        Object add4 = 100;
        Object add5 = 1;
        Object add6 = 101;
        Object add7 = 30;
        Object add8 = 50;
        Object add9 = 23;
        assertTrue(juLinkedList.remove(add1));
        assertTrue(juLinkedList.remove(add2));
        assertTrue(juLinkedList.remove(add3));
        assertTrue(juLinkedList.remove(add4));
        assertTrue(juLinkedList.remove(add5));
        assertFalse(juLinkedList.remove(add6));
        assertFalse(juLinkedList.remove(add7));
        assertFalse(juLinkedList.remove(add8));
        assertFalse(juLinkedList.remove(add9));
    }

    @Test
    public void testClear(){
        List<Integer> juLinkedList = new JULinkedList<>();
        juLinkedList.add(40);
        juLinkedList.add(29);
        juLinkedList.add(45);
        juLinkedList.add(100);
        juLinkedList.add(1);
        juLinkedList.clear();
        assertEquals(0, juLinkedList.size());
    }

    @Test
    public void testGet(){
        List<Integer> juLinkedList = new JULinkedList<>();
        juLinkedList.add(21);
        juLinkedList.add(34);
        juLinkedList.add(23);
        juLinkedList.add(45);
        juLinkedList.add(12);
        Integer number1 = 21;
        Integer number2 = 12;
        Integer number3 = 23;
        assertEquals(number1, juLinkedList.get(4));
        assertEquals(number2, juLinkedList.get(0));
        assertEquals(number3, juLinkedList.get(2));
    }

    @Test
    public void testRemove(){
        List<Integer> juLinkedList = new JULinkedList<>();
        juLinkedList.add(212);
        juLinkedList.add(123);
        juLinkedList.add(234);
        juLinkedList.add(345);
        juLinkedList.add(456);
        juLinkedList.add(567);
        juLinkedList.add(678);
        juLinkedList.add(789);
        assertEquals(8,juLinkedList.size());
        juLinkedList.remove(2);
        juLinkedList.remove(4);
        assertEquals(6, juLinkedList.size());
    }

    @Test
    public void testContains(){
        List<Integer> juLinkedList = new JULinkedList<>();
        juLinkedList.add(2);
        juLinkedList.add(1);
        juLinkedList.add(8);
        juLinkedList.add(3);
        assertTrue(juLinkedList.contains(2));
        assertTrue(juLinkedList.contains(1));
        assertTrue(juLinkedList.contains(8));
        assertTrue(juLinkedList.contains(3));

        assertFalse(juLinkedList.contains(12));
        assertFalse(juLinkedList.contains(23));
        assertFalse(juLinkedList.contains(34));
        assertFalse(juLinkedList.contains(45));
        assertFalse(juLinkedList.contains(56));
    }

    @Test
    public void testSet(){
        List<Integer> juLinkedList = new JULinkedList<>();

        juLinkedList.add(1);
        juLinkedList.add(2);
        juLinkedList.add(3);
        juLinkedList.add(4);
        juLinkedList.add(5);
        juLinkedList.add(6);

        juLinkedList.set(0,13);
        juLinkedList.set(1,24);
        juLinkedList.set(2,35);
        juLinkedList.set(3,47);
        juLinkedList.set(4,58);

        Integer number1 = 13;
        Integer number2 = 24;
        Integer number3 = 35;
        Integer number4 = 47;
        Integer number5 = 58;

        assertEquals(number1, juLinkedList.get(0));
        assertEquals(number2, juLinkedList.get(1));
        assertEquals(number3, juLinkedList.get(2));
        assertEquals(number4, juLinkedList.get(3));
        assertEquals(number5, juLinkedList.get(4));
    }

    @Test
    public void testIndexOf(){
        List<Integer> juLinkedList = new JULinkedList<>();

        juLinkedList.add(123);
        juLinkedList.add(456);
        juLinkedList.add(678);
        juLinkedList.add(890);

        assertEquals(3, juLinkedList.indexOf(123));
        assertEquals(2, juLinkedList.indexOf(456));
        assertEquals(1, juLinkedList.indexOf(678));
        assertEquals(0, juLinkedList.indexOf(890));
    }

    @Test
    public void testLastIndexOf(){
        List<Integer> juLinkedList = new JULinkedList<>();
        juLinkedList.add(12);
        juLinkedList.add(54);
        juLinkedList.add(45);
        juLinkedList.add(36);
        juLinkedList.add(11);
        juLinkedList.add(23);

        Object number1 = 100;
        Object number2 = 54;
        Object number3 = 45;
        Object number4 = 36;
        Object number5 = 11;
        Object number6 = 23;
        Object number7 = 12;

        assertEquals(-1, juLinkedList.lastIndexOf(number1));
        assertEquals(5, juLinkedList.lastIndexOf(number7));
        assertEquals(4, juLinkedList.lastIndexOf(number2));
        assertEquals(3, juLinkedList.lastIndexOf(number3));
        assertEquals(2, juLinkedList.lastIndexOf(number4));
        assertEquals(1, juLinkedList.lastIndexOf(number5));
        assertEquals(0, juLinkedList.lastIndexOf(number6));
    }

    @Test
    public void testAddIndexElement(){
        List<Integer> juLinkedList = new JULinkedList<>();

        Integer objective = 43;
        Integer objective1 = 4;
        Integer objective2 = 2;
        Integer objective3 = 3;

        juLinkedList.add(0,objective);
        juLinkedList.add(1,objective1);
        juLinkedList.add(2,objective2);
        juLinkedList.add(3,objective3);

        assertEquals(objective, juLinkedList.get(0));
    }
}

