package TareaEnClases;

public class Cola <T extends Comparable<T>>{

    //Declaración de atributos
    private Node<T> inicio;
    private Node<T> termino;

    //Constructor sin parametros
    public Cola() {
        inicio = null;
        termino = null;
    }

    //Metodo insertar
    public void insertar(int dato) {
        Node<T> node = new Node<>(dato);
        node.setNext(null);
        if (inicio == null & termino == null) {
            inicio = node;
            termino = node;
        }
        termino.setNext(node);
        termino = termino.getNext();
    }

    public int remover() {
        int dato=inicio.getDato();
        inicio=inicio.getNext();
        System.out.println(" Dato removido: "+dato);
        return dato;
    }

    @Override
    public String toString() {
        Node<T> node = this.inicio;
        String message = "";
        while (node != null) {
            message = message + node.toString();
            node = node.getNext();
        }
        return message;
    }

    public boolean repit(){
        if(inicio == null){
            return false;
        }else{
            for (int i = 0; i < inicio.getDato(); i++) {
                for (int j = 0; j < inicio.getDato(); j++) {
                    if (i == j){
                        return true;
                    }else if (i != j){
                        return false;
                    }
                }

            }

        }
        return false;
    }
}
