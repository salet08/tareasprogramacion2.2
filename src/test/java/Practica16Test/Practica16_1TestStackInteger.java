package Practica16Test;

import Practica16.Stack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Practica16_1TestStackInteger {
        @Test
        public void testIsEmptyInteger() {
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());
            list.push(1);
            list.push(2);
            list.push(3);
            list.push(4);
            list.push(5);
            assertFalse(list.isEmpty());
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            assertTrue(list.isEmpty());
            list.push(6);
            assertFalse(list.isEmpty());
            list.pop();
            assertTrue(list.isEmpty());
            list.push(7);
            list.push(8);
            list.push(9);
            assertFalse(list.isEmpty());
            list.pop();
            list.pop();
            list.pop();
            assertTrue(list.isEmpty());
            list.push(10);
            assertFalse(list.isEmpty());
        }

        @Test
        public void testPushInteger(){
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());
            list.push(100);
            list.push(120);
            list.push(130);
            assertEquals(3, list.size());
            list.push(140);
            assertEquals(4, list.size());
            list.push(150);
            list.push(160);
            list.push(170);
            list.push(180);
            list.push(190);
            assertEquals(9, list.size());
            list.push(1100);
            assertEquals(10, list.size());
            assertFalse(list.isEmpty());
        }


        @Test
        public void testIsPopInteger(){
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());

            list.push(12);
            list.push(23);
            list.push(45);
            list.push(67);
            list.push(89);
            assertEquals(5, list.size());
            list.pop();
            assertEquals(4, list.size());
            list.push(13);
            list.push(25);
            list.push(36);
            assertEquals(7, list.size());
            list.pop();
            list.pop();
            assertEquals(5, list.size());
            list.pop();
            assertEquals(4, list.size());
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            assertEquals(0, list.size());
        }

        /*********** CORRIGIENDO LOS TEST *****************/

        @Test
        public void testEmptyIntegerCorrected() {
            Stack<Integer> list = new Stack<>();
            assertTrue(list.isEmpty());
            list.push(1);
            list.push(2);
            list.push(3);
            list.push(4);
            list.push(5);
            assertEquals("Stack{ head={data=5, sig->{data=4, sig->{data=3, sig->{data=2, sig->{data=1, sig->null}}}}}, count=5}",list.toString());
            assertFalse(list.isEmpty());
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            list.pop();
            assertEquals("Stack{ head=null, count=0}",list.toString());
            assertTrue(list.isEmpty());
            list.push(6);
            assertEquals("Stack{ head={data=6, sig->null}, count=1}",list.toString());
            assertFalse(list.isEmpty());
            list.pop();
            assertEquals("Stack{ head=null, count=0}",list.toString());
            assertTrue(list.isEmpty());
            list.push(7);
            list.push(8);
            list.push(9);
            assertEquals("Stack{ head={data=9, sig->{data=8, sig->{data=7, sig->null}}}, count=3}",list.toString());
            assertFalse(list.isEmpty());
            list.pop();
            list.pop();
            list.pop();
            assertEquals("Stack{ head=null, count=0}",list.toString());
            assertTrue(list.isEmpty());
            list.push(10);
            assertEquals("Stack{ head={data=10, sig->null}, count=1}",list.toString());
            assertFalse(list.isEmpty());
        }

    @Test
    public void testPushIntegerCorrected(){
        Stack<Integer> list = new Stack<>();
        assertTrue(list.isEmpty());
        assertEquals("Stack{ head=null, count=0}",list.toString());
        list.push(100);
        list.push(120);
        list.push(130);
        assertEquals("Stack{ head={data=130, sig->{data=120, sig->{data=100, sig->null}}}, count=3}",list.toString());
        assertEquals(3, list.size());
        list.push(140);
        assertEquals("Stack{ head={data=140, sig->{data=130, sig->{data=120, sig->{data=100, sig->null}}}}, count=4}",list.toString());
        assertEquals(4, list.size());
        list.push(150);
        list.push(160);
        list.push(170);
        list.push(180);
        list.push(190);
        assertEquals("Stack{ head={data=190, sig->{data=180, sig->{data=170, sig->{data=160, sig->{data=150, sig->{data=140, sig->{data=130, sig->{data=120, sig->{data=100, sig->null}}}}}}}}}, count=9}",list.toString());
        assertEquals(9, list.size());
        list.push(1100);
        assertEquals("Stack{ head={data=1100, sig->{data=190, sig->{data=180, sig->{data=170, sig->{data=160, sig->{data=150, sig->{data=140, sig->{data=130, sig->{data=120, sig->{data=100, sig->null}}}}}}}}}}, count=10}",list.toString());
        assertEquals(10, list.size());
        assertFalse(list.isEmpty());
    }

        @Test
        public void testPopIntegerCorrected(){
            Stack<Integer>  list = new Stack<>();
            assertTrue(list.isEmpty());
            assertEquals("Stack{ head=null, count=0}",list.toString());

            list.push(11);
            list.push(12);
            list.push(13);
            list.push(14);
            assertEquals("Stack{ head={data=14, sig->{data=13, sig->{data=12, sig->{data=11, sig->null}}}}, count=4}",list.toString());

            assertFalse(list.isEmpty());
            assertEquals(4, list.size());
            assertEquals(14,list.peek());
            assertFalse(list.isEmpty());
            list.pop();
            assertEquals("Stack{ head={data=13, sig->{data=12, sig->{data=11, sig->null}}}, count=3}",list.toString());
            assertEquals(13,list.peek());
            assertEquals(3, list.size());
            assertFalse(list.isEmpty());
            list.pop();
            assertEquals("Stack{ head={data=12, sig->{data=11, sig->null}}, count=2}",list.toString());
            assertEquals(12,list.peek());
            assertEquals(2, list.size());
            assertFalse(list.isEmpty());
            list.pop();
            assertEquals("Stack{ head={data=11, sig->null}, count=1}",list.toString());
            assertEquals(11, list.peek());
            assertEquals(1, list.size());
            assertFalse(list.isEmpty());
            list.pop();
            assertEquals("Stack{ head=null, count=0}",list.toString());
            assertEquals(0, list.size());
            assertTrue(list.isEmpty());
        }
}
