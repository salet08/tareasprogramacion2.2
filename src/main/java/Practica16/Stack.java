package Practica16;
public class Stack <T extends Comparable<T>>{
    private Node<T> head;
    private int count;

    public Stack() {
        this.head = null;
        this.count = 0;
    }

    public void push(T nodeData) {
        Node<T> node = new Node<>(nodeData);
        node.setNext(head);
        head = node;
        count ++;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public T pop(){
        if (isEmpty()) {
            System.exit(-1);
        }
        T higher = peek();
        count --;
        head = head.getNext();
        return higher;
    }

    public T peek() {
        if (isEmpty()) {
            System.exit(1);
        }
        return head.getData();
    }

    public int size() {
        return count;
    }

    @Override
    public String toString() {
        return "Stack{" +
                " head=" + head +
                ", count=" + count +
                '}';
    }
}
