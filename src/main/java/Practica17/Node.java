package Practica17;

public class Node <T>{
    private T data;
    private Node<T> next;
    public Node(T data) {
        this.data = data;
        next = null;
    }

    public Node() {

    }

    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return data + ", "+ next;
    }
}
