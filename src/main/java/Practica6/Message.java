package Practica6;
public class Message {
    public void bienvenida() {
        System.out.println("¡¡Bienvenido al juego!!");
    }

    public void introducePersonaje() {
        System.out.print("Introduce cuantos personajes quieres utilizar " +
                "1 o 2: ");
    }

    public void opcionJuego() {
        System.out.print("\n\tMENU" +
                "\n1 . Mover hacia arriba ▲" +
                "\n2 . Mover hacia moveDown ▼" +
                "\n3 . Mover a la moveRight ►" +
                "\n4 . Mover a la izquiera ◄\n" +
                "►► ");
    }

    public void opcionIncorrecta() {
        System.out.println("\nLa option no existe , vuelva a digitar");
    }

    public void showFinishGame() {
        System.out.println("Felicidades, ¡¡GANASTE!!");
    }

}
