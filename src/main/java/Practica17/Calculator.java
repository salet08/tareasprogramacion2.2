package Practica17;

public class Calculator {

    private final Stack<Character> stack;

    public Calculator(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        stack = reverse(tokens);
    }

    public Stack<Character> getTokens(String stack) {//1+2
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < stack.length(); i++) {
            tokens.push(stack.charAt(i));
        }
        return tokens;
    }

    private static Stack<Character> sum(Stack<Character> number1, Stack<Character> number2) {
        Stack<Character> result = new Stack<>();
        int number = 0, addition;
        while (number1.size() > 0 && number2.size() > 0) {
            addition = (number + Character.getNumericValue(number1.pop()) + Character.getNumericValue(number2.pop()));
            result.push((char) (addition % 10 + '0'));
            number = addition / 10;
        }
        while (number1.size() > 0) {
            addition = (number + Character.getNumericValue(number1.pop()));
            result.push((char) (addition % 10 + '0'));
            number = addition / 10;
        }
        while (number2.size() > 0) {
            addition = (number + Character.getNumericValue(number2.pop()));
            result.push((char) (addition % 10 + '0'));
            number = addition / 10;
        }
        while (number > 0) {
            result.push((char) (number + '0'));
            number /= 10;
        }
        result = reverse(result);
        return result;
    }

    public int operationSum(String numberOne, String numberTwo) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < numberOne.length(); i++) {
            stack.push(numberOne.charAt(i));
        }
        Stack<Character> twoTokens = new Stack<>();
        for (int i = 0; i < numberTwo.length(); i++) {
            twoTokens.push(numberTwo.charAt(i));
        }
        Stack<Character> result = reverse(sum(stack, twoTokens));
        String resultFinalString = "";
        while (result.size() > 0) {
            resultFinalString += result.pop();
        }
        return Integer.parseInt(resultFinalString);
    }

    private static Stack<Character> reverse(Stack<Character> model) {
        Stack<Character> result = new Stack<>();
        while (model.size() > 0) {
            result.push(model.pop());
        }
        return result;
    }

    public int operationPow(String operation) {
        int resultado = 0;
        Stack<Character> tokens = getTokens(operation);
        Character number1 = tokens.pop();
        Character character = '^';
        while (tokens.size() > 0) {
            Node node1 = new Node<>(tokens.pop());
            if (node1.getData().equals(character)) {
                Character number2 = tokens.pop();
                resultado += (int) Math.pow(Integer.parseInt(String.valueOf(number2)), Integer.parseInt(String.valueOf(number1)));
            }
            Character auxiliary = '0';
            number1 = auxiliary;
        }
        return resultado;
    }

    private static Stack<Character> factorial(Stack<Character> number) {
        Stack<Character> result = new Stack<>();
        Stack<Character> copy = new Stack<>();
        result.push('1');
        while (number.size() > 0) {
            copyStack(number, copy);
            result = multiplicacion(result, copy);
            number = subtractOneFromStack(number);
        }
        return result;
    }

    public int operationFactorial(String number) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < number.length(); i++) {
            tokens.push(number.charAt(i));
        }
        Stack<Character> result = reverse(factorial(tokens));
        String resultFinalString = "";
        while (result.size() > 0) {
            resultFinalString += result.pop();
        }
        return Integer.parseInt(resultFinalString);
    }

    private static Stack<Character> multiplicacion(Stack<Character> number1, Stack<Character> number2) {
        Stack<Character> copy = new Stack<>();
        Stack<Character> result = new Stack<>();
        Stack<Character> stack = new Stack<>();
        int counter = 0;
        while (number2.size() > 0) {
            int value = Character.getNumericValue(number2.pop());
            while (value > 0) {
                copyStack(number1, copy);
                stack = sum(stack, copy);
                value--;
            }
            if (counter > 0) {
                for (int i = 0; i < counter; i++) {
                    stack.push('0');
                }
            }
            result = sum(result, stack);
            stack = new Stack<>();
            counter++;
        }
        return result;
    }

    public int operationMultiplication(String numberOne, String numberTwo) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < numberOne.length(); i++) {
            tokens.push(numberOne.charAt(i));
        }
        Stack<Character> twoTokens = new Stack<>();
        for (int i = 0; i < numberTwo.length(); i++) {
            twoTokens.push(numberTwo.charAt(i));
        }
        Stack<Character> result = reverse(multiplicacion(tokens, twoTokens));
        String finalString = "";
        while (result.size() > 0) {
            finalString += result.pop();
        }
        return Integer.parseInt(finalString);
    }

    private static void copyStack(Stack<Character> number1, Stack<Character> numberCopy) {
        Stack<Character> temp = new Stack<>();
        while (number1.size() > 0) {
            temp.push(number1.pop());
        }
        while (temp.size() > 0) {
            char character = temp.pop();
            number1.push(character);
            numberCopy.push(character);
        }
    }

    private static Stack<Character> subtractOneFromStack(Stack<Character> digits) {
        Stack<Character> result = new Stack<>();
        copyStack(digits, result);
        int last = Character.getNumericValue(result.pop());
        if (digits.size() == 1 && last == 1) {
            return new Stack<>();
        }
        if (last > 0) {
            result.push((char) ((last - 1) + '0'));
        } else {
            Stack<Character> stack = new Stack<>();
            result.push('0');
            boolean continueAdding = true;
            char auxiliary = 0;
            while (continueAdding) {
                char character = result.pop();
                if (character == '0') {
                    stack.push('9');
                } else {
                    auxiliary = character;
                    continueAdding = false;
                }
            }
            int after = Character.getNumericValue(auxiliary);
            result.push((char) (after - 1 + '0'));
            while (stack.size() > 0) {
                result.push(stack.pop());
            }
        }
        result = reverse(result);
        Stack<Character> copy = new Stack<>();
        copyStack(result,copy);
        if (copy.pop()=='0'){
            result.pop();
        }
        result = reverse(result);
        return result;
    }
}
