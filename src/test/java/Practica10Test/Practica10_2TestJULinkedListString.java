package Practica10Test;

import Practica10.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Practica10_2TestJULinkedListString {

    @Test
    public void testSize() {
        List<String> juLinkedList = new JULinkedList<>();
        assertEquals(0, juLinkedList.size());
        juLinkedList.add("2");
        juLinkedList.add("55");
        assertEquals(2, juLinkedList.size());
        juLinkedList.add("5");
        juLinkedList.add("0");
        juLinkedList.add("2");
        juLinkedList.add("1");
        juLinkedList.add("23");
        assertEquals(7, juLinkedList.size());
    }

    @Test
    public void testisEmpty() {
        List<String> juLinkedList = new JULinkedList<>();
        assertTrue(juLinkedList.isEmpty());
        juLinkedList.add("40");
        juLinkedList.add("29");
        juLinkedList.add("45");
        juLinkedList.add("100");
        juLinkedList.add("1");
        assertFalse(juLinkedList.isEmpty());
    }

    @Test
    public void testIterator(){
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("40");
        juLinkedList.add("29");

        int counter = 0;
        while (juLinkedList.iterator().hasNext()){
            assertEquals(juLinkedList.get(counter), juLinkedList.iterator().next());
            counter++;
            break;
        }
        counter++;
        assertEquals(2, counter);
    }

    @Test
    public void testAdd() {
        List<String> juLinkedList = new JULinkedList<>();
        assertTrue(juLinkedList.add("1"));
        assertEquals(1, juLinkedList.size());
        assertTrue(juLinkedList.add("2"));
        assertTrue(juLinkedList.add("3"));
        assertEquals(3, juLinkedList.size());
        assertTrue(juLinkedList.add("4"));
        assertTrue(juLinkedList.add("5"));
        assertTrue(juLinkedList.add("6"));
        assertTrue(juLinkedList.add("7"));
        assertTrue(juLinkedList.add("8"));
        assertEquals(8, juLinkedList.size());
        assertTrue(juLinkedList.add("9"));
        assertEquals(9, juLinkedList.size());

        juLinkedList.add("6");
        juLinkedList.add("2");
        juLinkedList.add("1");
        juLinkedList.add("4");
        assertTrue(juLinkedList.add("6"));
        assertTrue(juLinkedList.add("2"));
        assertTrue(juLinkedList.add("1"));
        assertTrue(juLinkedList.add("4"));
    }

    @Test
    public void testRemoveObject() {
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("40");
        juLinkedList.add("29");
        juLinkedList.add("45");
        juLinkedList.add("100");
        juLinkedList.add("1");

        assertTrue(juLinkedList.remove("40"));
        assertTrue(juLinkedList.remove("29"));
        assertTrue(juLinkedList.remove("45"));
        assertTrue(juLinkedList.remove("100"));
        assertTrue(juLinkedList.remove("1"));
        assertFalse(juLinkedList.remove("101"));
        assertFalse(juLinkedList.remove("30"));
        assertFalse(juLinkedList.remove("50"));
        assertFalse(juLinkedList.remove("23"));
    }

    @Test
    public void testClear() {
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("40");
        juLinkedList.add("29");
        juLinkedList.add("45");
        juLinkedList.add("100");
        juLinkedList.add("1");
        juLinkedList.clear();
        assertEquals(0, juLinkedList.size());
    }

    @Test
    public void testGet() {
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("21");
        juLinkedList.add("34");
        juLinkedList.add("23");
        juLinkedList.add("45");
        juLinkedList.add("12");
        assertEquals("21", juLinkedList.get(4));
        assertEquals("12", juLinkedList.get(0));
        assertEquals("23", juLinkedList.get(2));
    }

    @Test
    public void testRemove() {
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("212");
        juLinkedList.add("123");
        juLinkedList.add("234");
        juLinkedList.add("345");
        juLinkedList.add("456");
        juLinkedList.add("567");
        juLinkedList.add("678");
        juLinkedList.add("789");
        assertEquals(8, juLinkedList.size());
        juLinkedList.remove(2);
        juLinkedList.remove(4);
        assertEquals(6, juLinkedList.size());
    }

    @Test
    public void testContains() {
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("2");
        juLinkedList.add("1");
        juLinkedList.add("8");
        juLinkedList.add("3");
        assertTrue(juLinkedList.contains("2"));
        assertTrue(juLinkedList.contains("1"));
        assertTrue(juLinkedList.contains("8"));
        assertTrue(juLinkedList.contains("3"));

        assertFalse(juLinkedList.contains("12"));
        assertFalse(juLinkedList.contains("23"));
        assertFalse(juLinkedList.contains("34"));
        assertFalse(juLinkedList.contains("45"));
        assertFalse(juLinkedList.contains("56"));
    }

    @Test
    public void testSet() {
        List<String> juLinkedList = new JULinkedList<>();

        juLinkedList.add("1");
        juLinkedList.add("2");
        juLinkedList.add("3");
        juLinkedList.add("4");
        juLinkedList.add("5");
        juLinkedList.add("6");

        juLinkedList.set(0, "13");
        juLinkedList.set(1, "24");
        juLinkedList.set(2, "35");
        juLinkedList.set(3, "47");
        juLinkedList.set(4, "58");

        assertEquals("13", juLinkedList.get(0));
        assertEquals("24", juLinkedList.get(1));
        assertEquals("35", juLinkedList.get(2));
        assertEquals("47", juLinkedList.get(3));
        assertEquals("58", juLinkedList.get(4));
    }

    @Test
    public void testIndexOf() {
        List<String> juLinkedList = new JULinkedList<>();

        juLinkedList.add("123");
        juLinkedList.add("456");
        juLinkedList.add("678");
        juLinkedList.add("890");

        assertEquals(3, juLinkedList.indexOf("123"));
        assertEquals(2, juLinkedList.indexOf("456"));
        assertEquals(1, juLinkedList.indexOf("678"));
        assertEquals(0, juLinkedList.indexOf("890"));
    }

    @Test
    public void testLastIndexOf() {
        List<String> juLinkedList = new JULinkedList<>();
        juLinkedList.add("12");
        juLinkedList.add("54");
        juLinkedList.add("45");
        juLinkedList.add("36");
        juLinkedList.add("11");
        juLinkedList.add("23");

        Object number1 = "100";
        Object number2 = "54";
        Object number3 = "45";
        Object number4 = "36";
        Object number5 = "11";
        Object number6 = "23";
        Object number7 = "12";

        assertEquals(-1, juLinkedList.lastIndexOf(number1));
        assertEquals(5, juLinkedList.lastIndexOf(number7));
        assertEquals(4, juLinkedList.lastIndexOf(number2));
        assertEquals(3, juLinkedList.lastIndexOf(number3));
        assertEquals(2, juLinkedList.lastIndexOf(number4));
        assertEquals(1, juLinkedList.lastIndexOf(number5));
        assertEquals(0, juLinkedList.lastIndexOf(number6));
    }

    @Test
    public void testAddIndexElement() {
        List<String> juLinkedList = new JULinkedList<>();

        juLinkedList.add(0,"A");
        juLinkedList.add(1,"B");
        juLinkedList.add(2,"C");
        juLinkedList.add(3,"D");

        assertEquals("A", juLinkedList.get(0));
    }
}