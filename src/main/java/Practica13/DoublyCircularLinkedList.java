package Practica13;
public class DoublyCircularLinkedList<T> implements List<T> {

    Node<T> head;
    int length;

    public void setHead(Node<T> head) {
        this.head = head;
    }

    @Override
    public int size() {
        Node<T> newHead = head;
        int count = 0;
        if (head != null) {
            do {
                newHead = newHead.getNext();
                count ++;
            } while (newHead != head);
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T get(int index) {
        Node<T> node = head;
        if (isEmpty() || index < 0) return  null;
        else {
            for (int i = 0; i < index; i++) {
                node = node.getNext();
            }
        }
        return node.getData();

    }

    @Override
    public void mergeSort() {
        Node<T> node = head;
        inicializateMergeSort(node);
    }

    private Node<T> inicializateMergeSort(Node<T> head){
        if (head == null || head.getNext() == null) {
            return head;
        }
        // split head into `a` and `b` sublists
        Node<T> a = head, b;

        Node<T> slow = split(head);
        b = slow.getNext();
        slow.setNext(null);

        // recursively sort the sublists
        a = inicializateMergeSort(a);
        b = inicializateMergeSort(b);

        // merge the two sorted lists
        head = merge(a, b);
        return head;
    }

    // Function to merge two linked lists
    private Node<T> merge(Node<T> first, Node<T> second) {
        // If first linked list is empty
        if (first == null) {
            return second;
        }
        // If second linked list is empty
        if (second == null) {
            return first;
        }
        int firstDate = (int) first.getData();
        int secondDate = (int) second.getData();
        // Pick the smaller value
        if (firstDate <= secondDate) {
            first.setNext(merge(first.getNext(), second));
            first.getNext().setPrevious(first);
            first.setPrevious(null);
            return first;
        } else {
            second.setNext(merge(first, second.getNext()));
            second.getNext().setPrevious(second);
            second.setPrevious(null);
            return second;
        }
    }

    private Node<T> split(Node<T> head) {
        Node<T> fast = head.getNext(), slow = head;
        while (fast != null)
        {
            fast = fast.getNext();
            if (fast != null)
            {
                slow = slow.getNext();
                fast = fast.getNext();
            }
        }
        System.out.println(" slow: "+ slow);
        return slow;
    }

    @Override
    public int compareTo(T o) {
        return 0;
    }

}
