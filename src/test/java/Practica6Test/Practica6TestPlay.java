package Practica6Test;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import Practica6.*;
public class Practica6TestPlay {
    Play play = new Play(10, 10);
    private static final String INITIAL_BOARD_FOR_TEST = "—————＊————\n" +
            "—O————————\n" +
            "——————＊———\n" +
            "——————————\n" +
            "——————————\n" +
            "——＊———————\n" +
            "——————————\n" +
            "—————＊————\n" +
            "——————————\n" +
            "————————＊End\n";

    @Test
    public void fillArrayTest() {
        play.fillArray();

        assertEquals("——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "—————————End\n", play.getBoardOnString());
    }

    @Test
    public void getBoardOnStringTest() {
        play.fillArray();
        String expected = "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "—————————End\n";

        assertEquals(expected, play.getBoardOnString());
    }

    @Test
    public void moveDownPlayerPositionOnBoard() {
        play.fillArray();
        play.fillForTesting();

        assertEquals(INITIAL_BOARD_FOR_TEST, play.getBoardOnString());

        play.moveDown();

        assertEquals("—————＊————\n" +
                "——————————\n" +
                "—O————＊———\n" +
                "——————————\n" +
                "——————————\n" +
                "——＊———————\n" +
                "——————————\n" +
                "—————＊————\n" +
                "——————————\n" +
                "————————＊End\n", play.getBoardOnString());
    }


    @Test
    public void moveUpPlayerPositionOnBoard() {
        play.fillArray();
        play.fillForTesting();

        assertEquals(INITIAL_BOARD_FOR_TEST, play.getBoardOnString());

        play.moveUp();
        assertEquals("—O———＊————\n" +
                "——————————\n" +
                "——————＊———\n" +
                "——————————\n" +
                "——————————\n" +
                "——＊———————\n" +
                "——————————\n" +
                "—————＊————\n" +
                "——————————\n" +
                "————————＊End\n", play.getBoardOnString());
    }



    @Test
    public void moveLeftPlayerPositiononOnBoard() {
        play.fillArray();
        play.fillForTesting();

        assertEquals(INITIAL_BOARD_FOR_TEST, play.getBoardOnString());

        play.moveLeft();
        assertEquals("—————＊————\n" +
                "O—————————\n" +
                "——————＊———\n" +
                "——————————\n" +
                "——————————\n" +
                "——＊———————\n" +
                "——————————\n" +
                "—————＊————\n" +
                "——————————\n" +
                "————————＊End\n", play.getBoardOnString());
    }

    @Test
    public void moveRightPlayerPositiononOnBoard() {
        play.fillArray();
        play.fillForTesting();

        assertEquals(INITIAL_BOARD_FOR_TEST, play.getBoardOnString());

        play.moveRight();
        assertEquals("—————＊————\n" +
                "——O———————\n" +
                "——————＊———\n" +
                "——————————\n" +
                "——————————\n" +
                "——＊———————\n" +
                "——————————\n" +
                "—————＊————\n" +
                "——————————\n" +
                "————————＊End\n", play.getBoardOnString());
    }

    @Test
    public void setPositionTest() {
        int x = 2;
        int y = 2;
        String item = "＊";

        String[][] arrayAux = new String[10][10];
        arrayAux[y][x] = item;

        play.fillArray();
        play.setPosition(x, y, item);

        assertEquals("——————————\n" +
                "——————————\n" +
                "——＊———————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "——————————\n" +
                "—————————End\n", play.getBoardOnString());
    }
}
