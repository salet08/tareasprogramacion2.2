package Practica13Test;

import Practica13.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Practica13TestDoublyCircularLinkedList {
    @Test
    public void testSize(){
        DoublyCircularLinkedList doublyLinkedList = new DoublyCircularLinkedList();

        Node<Integer> node1 = new Node<>(3);
        Node<Integer> node2 = new Node<>(8);
        Node<Integer> node3 = new Node<>(5);
        Node<Integer> node4 = new Node<>(9);
        Node<Integer> node5 = new Node<>(2);

        doublyLinkedList.setHead(node1);
        node1.setNext(node2);
        node1.setPrevious(node5);
        node2.setNext(node3);
        node2.setPrevious(node1);
        node3.setNext(node4);
        node3.setPrevious(node2);
        node4.setNext(node5);
        node4.setPrevious(node3);
        node5.setNext(node1);
        node5.setPrevious(node4);

        assertEquals(5, doublyLinkedList.size());

    }

    @Test
    public void testIsEmpty(){
        DoublyCircularLinkedList doublyLinkedList = new DoublyCircularLinkedList();
        assertTrue(doublyLinkedList.isEmpty());

        Node<Integer> node1 = new Node<>(3);
        Node<Integer> node2 = new Node<>(8);
        Node<Integer> node3 = new Node<>(5);
        Node<Integer> node4 = new Node<>(9);
        Node<Integer> node5 = new Node<>(2);

        doublyLinkedList.setHead(node1);
        node1.setNext(node2);
        node1.setPrevious(node5);
        node2.setNext(node3);
        node2.setPrevious(node1);
        node3.setNext(node4);
        node3.setPrevious(node2);
        node4.setNext(node5);
        node4.setPrevious(node3);
        node5.setNext(node1);
        node5.setPrevious(node4);

        assertFalse(doublyLinkedList.isEmpty());
    }

    @Test
    public void testGet(){
        DoublyCircularLinkedList list = new DoublyCircularLinkedList();

        Node<Integer> node1 = new Node<>(3);
        Node<Integer> node2 = new Node<>(8);
        Node<Integer> node3 = new Node<>(5);
        Node<Integer> node4 = new Node<>(9);
        Node<Integer> node5 = new Node<>(2);

        list.setHead(node1);
        node1.setNext(node2);
        node1.setPrevious(node5);
        node2.setNext(node3);
        node2.setPrevious(node1);
        node3.setNext(node4);
        node3.setPrevious(node2);
        node4.setNext(node5);
        node4.setPrevious(node3);
        node5.setNext(node1);
        node5.setPrevious(node4);

        Object number1 = 2;
        Object number2 = 8;
        Object number3 = 9;

        assertEquals(number1, list.get(4));
        assertEquals(number2, list.get(1));
        assertEquals(number3, list.get(3));

    }

    /*@Test
    public void testMergeSort() {
        DoublyCircularLinkedList List = new DoublyCircularLinkedList();

        Node<Integer> node1 = new Node<>(3);
        Node<Integer> node2 = new Node<>(8);
        Node<Integer> node3 = new Node<>(5);
        Node<Integer> node4 = new Node<>(9);
        Node<Integer> node5 = new Node<>(2);

        List.setHead(node1);
        node1.setNext(node2);
        node1.setPrevious(node5);
        node2.setNext(node3);
        node2.setPrevious(node1);
        node3.setNext(node4);
        node3.setPrevious(node2);
        node4.setNext(node5);
        node4.setPrevious(node3);
        node5.setNext(node1);
        node5.setPrevious(node4);
        List.mergeSort();
        assertEquals(5, List);
    }*/
}
