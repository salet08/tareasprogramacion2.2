package Practica15;
public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();
        //LlamadasEncadenadas.doFour();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion:.................
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasando un número entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion:.................
        //Contador.contarHasta(5);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));


    }
}
