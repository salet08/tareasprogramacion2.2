package Practica6;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

/*
* Tarea:
Implementar un juego que use un mapa (10x10) de coordenadas X Y,
* donde un jugador será ubicado (en una casilla) en una posición aleatoria
* dentro el mapa, luego 5 obstáculos dentro el mapa en posiciones
* aleatorias, similar que el jugador intenta llegar a la esquina inferior
* moveRight del mapa, un paso a la vez, pasando de su posición actual a la
* moveRight, moveLeft, arriba, moveDown. Siempre y cuando no se encuentre
* con un obstáculo.
* Tomar en cuenta en el diseño (no en la implementación) los siguientes
* puntos: que a futuro los obstáculos, podrían moverse; la posibilidad de
* más de un jugador.
* Enviar en un Merge Request (MR) el diseño UML (captura de pantalla)
* de la solución y el código, para el código se evaluará la facilidad al
* momento de leerlo y de ser actualizado con nuevas características.
* Implementar las pruebas unitarias que considere importantes.
* */

public class Play {
    private String[][] array;
    int x, y, option;
    private Random rand = new Random();
    Player player = new Player();
    Obstacle obstaculo = new Obstacle();
    Scanner scanner = new Scanner(System.in);
    Message message = new Message();
    private final String charObstacle ="＊";
    private final String charPlayer = "O";

    public Play(int x, int y) {
        this.x = x;
        this.y = y;
        array = new String[x][y];
    }
    public String[][] setPosition(int x, int y, String string){
        array[x][y] = string;
        return array;
    }

    public void fillArray() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = "—";
                array[x - 1][y - 1] = "End";
            }
        }
    }

    public void showArray() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                System.out.print(array[i][j] + "    ");
            }
            System.out.println();
        }
    }
    public String getBoardOnString(){
        StringBuilder boardS = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                boardS.append(array[i][j]);
            }
            boardS.append("\n");
        }
        return boardS.toString();
    }
    public void fillForTesting(){
        setPosition(0,5,charObstacle);
        setPosition(2,6,charObstacle);
        setPosition(5,2,charObstacle);
        setPosition(7,5,charObstacle);
        setPosition(9,8,charObstacle);
        setPosition(1,1,charPlayer);
        player.setPositionY(1);
        player.setPositionX(1);
    }


    public void assignPlayerPosition() {
        int randomNumX = rand.nextInt(array.length);
        int randomNumY = rand.nextInt(array.length);
        player.setPositionX(randomNumX);
        player.setPositionY(randomNumY);
        array[randomNumX][randomNumY] = "O";
    }

    public void asignObstaculoPosition(int numeroObstaculos) {
        while (numeroObstaculos > 0) {
            int randomNumX = rand.nextInt(array.length);
            int randomNumY = rand.nextInt(array.length);
            obstaculo.setPositionX(randomNumX);
            obstaculo.setPositionY(randomNumY);
            array[randomNumX][randomNumY] = "＊";
            numeroObstaculos--;
        }
    }

    public void showMethods() {
        fillArray();
        assignPlayerPosition();
        asignObstaculoPosition(5);
        showArray();
        gameWon();
    }

    private int[] playerCoordinates() {
        int[] coordenadasdeplayer = new int[2];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (Objects.equals(array[i][j], "O")) {
                    coordenadasdeplayer[0] = j;
                    coordenadasdeplayer[1] = i;
                }
            }
        }
        return coordenadasdeplayer;
    }

    public void gameOption() {
        message.opcionJuego();
        option = scanner.nextInt();
        switch (option) {
            case 1:
                moveUp();
                break;
            case 2:
                moveDown();
                break;
            case 3:
                moveRight();
                break;
            case 4:
                moveLeft();
                break;
            default:
                message.opcionIncorrecta();
                break;
        }
        gameWon();
    }

    public void moveUp(){
        int playerX = playerCoordinates()[0];
        int playerY = playerCoordinates()[1];
        if (playerY != 0 && !array[playerY - 1][playerX].equals("＊")) {
            array[playerY - 1][playerX] = "O";
            array[playerY][playerX] = "—";
        }
    }

    public void moveDown() {
        int playerX = playerCoordinates()[0];
        int playerY = playerCoordinates()[1];
        if (playerY != array.length - 1 && !array[playerY + 1][playerX].equals("＊")) {
            array[playerY + 1][playerX] = "O";
            array[playerY][playerX] = "—";
        }
    }

    public void moveLeft() {
        int playerX = playerCoordinates()[0];
        int playerY = playerCoordinates()[1];
        if (playerX != 0 && !array[playerY][playerX - 1].equals("＊")) {
            array[playerY][playerX - 1] = "O";
            array[playerY][playerX] = "—";
        }
    }

    public void moveRight() {
        int playerX = playerCoordinates()[0];
        int playerY = playerCoordinates()[1];
        if (playerX != array[0].length - 1 && !array[playerY][playerX + 1].equals("＊")) {
            array[playerY][playerX + 1] = "O";
            array[playerY][playerX] = "—";
        }
    }

    public void gameWon() {
        int playerX = playerCoordinates()[0];
        int playerY = playerCoordinates()[1];

        if (array[playerY][playerX]==array[9][9]) {
            message.showFinishGame();
        }
        else {
            gameOption();
        }
    }
}