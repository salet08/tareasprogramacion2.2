package Practica16Test;

import Practica16.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Practica16TestStackString {

    @Test
    public void testIsEmptyString(){
        Stack<String> list = new Stack<>();
        assertTrue(list.isEmpty());
        list.push("1");
        list.push("2");
        list.push("3");
        list.push("4");
        list.push("5");
        assertFalse(list.isEmpty());
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
        list.push("David");
        assertFalse(list.isEmpty());
        list.pop();
        assertTrue(list.isEmpty());
        list.push("Diego");
        list.push("Maria");
        list.push("Jefersson");
        assertFalse(list.isEmpty());
        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
        list.push("Gaby");
        list.push("Reyes");
        list.push("Midoria");
        list.push("Izuku");
        assertFalse(list.isEmpty());
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
    }

    @Test
    public void testIsPushString(){
        Stack<String> list = new Stack<>();
        assertEquals(0, list.size());

        list.push("Maria");
        list.push("Jose");
        list.push("Teo");
        list.push("Carmen");
        assertEquals(4, list.size());

        list.push("Shoto");
        assertEquals(5, list.size());

        list.push("Ash");
        list.push("Eiji");
        list.push("Baji");
        assertEquals(8, list.size());

        list.push("Micke");
        list.push("Mario");
        assertEquals(10, list.size());
    }

    @Test
    public void testIsPopString(){
        Stack<String> list = new Stack<>();
        assertEquals(0, list.size());
        list.push("Pelota");
        list.push("Arbol");
        list.push("Mesa");
        list.push("Silla");
        assertFalse(list.isEmpty());
        assertEquals(4, list.size());

        assertFalse(list.isEmpty());
        list.pop();
        assertEquals(3, list.size());

        list.push("A");
        list.push("B");
        list.push("C");
        assertEquals(6, list.size());

        list.pop();
        assertEquals(5, list.size());
        list.push("D");
        list.push("E");
        list.push("F");
        list.push("G");
        list.push("H");
        assertEquals(10, list.size());

        list.pop();
        assertEquals(9, list.size());

        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertEquals(3, list.size());

        list.pop();
        list.pop();
        list.pop();
        assertTrue(list.isEmpty());
    }

    /*********** CORRIGIENDO LOS TEST *****************/

    @Test
    public void testIsEmptyStringCorrected(){
        Stack<String> list = new Stack<>();
        assertTrue(list.isEmpty());
        assertEquals("Stack{ head=null, count=0}",list.toString());
        list.push("1");
        list.push("2");
        list.push("3");
        list.push("4");
        list.push("5");
        assertFalse(list.isEmpty());
        assertEquals("Stack{ head={data=5, sig->{data=4, sig->{data=3, sig->{data=2, sig->{data=1, sig->null}}}}}, count=5}",list.toString());
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertEquals("Stack{ head=null, count=0}",list.toString());
        assertTrue(list.isEmpty());
        list.push("David");
        assertFalse(list.isEmpty());
        list.pop();
        assertEquals("Stack{ head=null, count=0}",list.toString());
        assertTrue(list.isEmpty());
        list.push("Diego");
        list.push("Maria");
        list.push("Jefersson");
        assertFalse(list.isEmpty());
        assertEquals("Stack{ head={data=Jefersson, sig->{data=Maria, sig->{data=Diego, sig->null}}}, count=3}",list.toString());
        list.pop();
        list.pop();
        list.pop();
        assertEquals("Stack{ head=null, count=0}",list.toString());
        assertTrue(list.isEmpty());
        list.push("Gaby");
        list.push("Reyes");
        list.push("Midoria");
        list.push("Izuku");
        assertFalse(list.isEmpty());
        assertEquals("Stack{ head={data=Izuku, sig->{data=Midoria, sig->{data=Reyes, sig->{data=Gaby, sig->null}}}}, count=4}",list.toString());
        list.pop();
        list.pop();
        list.pop();
        list.pop();
        assertEquals("Stack{ head=null, count=0}",list.toString());
        assertTrue(list.isEmpty());
    }

    @Test
    public void testIsPushStringCorrected(){
        Stack<String> list = new Stack<>();
        assertEquals(0, list.size());
        assertEquals("Stack{ head=null, count=0}",list.toString());

        list.push("Maria");
        list.push("Jose");
        list.push("Teo");
        list.push("Carmen");
        assertEquals(4, list.size());
        assertEquals("Stack{ head={data=Carmen, sig->{data=Teo, sig->{data=Jose, sig->{data=Maria, sig->null}}}}, count=4}",list.toString());

        list.push("Shoto");
        assertEquals(5, list.size());
        assertEquals("Stack{ head={data=Shoto, sig->{data=Carmen, sig->{data=Teo, sig->{data=Jose, sig->{data=Maria, sig->null}}}}}, count=5}",list.toString());

        list.push("Ash");
        list.push("Eiji");
        list.push("Baji");
        assertEquals(8, list.size());
        assertEquals("Stack{ head={data=Baji, sig->{data=Eiji, sig->{data=Ash, sig->{data=Shoto, sig->{data=Carmen, sig->{data=Teo, sig->{data=Jose, sig->{data=Maria, sig->null}}}}}}}}, count=8}",list.toString());

        list.push("Micke");
        list.push("Mario");
        assertEquals(10, list.size());
        assertEquals("Stack{ head={data=Mario, sig->{data=Micke, sig->{data=Baji, sig->{data=Eiji, sig->{data=Ash, sig->{data=Shoto, sig->{data=Carmen, sig->{data=Teo, sig->{data=Jose, sig->{data=Maria, sig->null}}}}}}}}}}, count=10}",list.toString());
    }

    @Test
    public void testPopIntegerCorrected(){
        Stack<String>  list = new Stack<>();
        assertTrue(list.isEmpty());
        assertEquals("Stack{ head=null, count=0}",list.toString());

        list.push("aa");
        list.push("bb");
        list.push("cc");
        list.push("dd");

        assertFalse(list.isEmpty());
        assertEquals(4, list.size());
        assertEquals("dd",list.peek());
        assertFalse(list.isEmpty());
        list.pop();
        assertEquals("Stack{ head={data=cc, sig->{data=bb, sig->{data=aa, sig->null}}}, count=3}",list.toString());
        assertEquals("cc",list.peek());
        assertEquals(3, list.size());
        assertFalse(list.isEmpty());
        list.pop();
        assertEquals("Stack{ head={data=bb, sig->{data=aa, sig->null}}, count=2}",list.toString());
        assertEquals("bb",list.peek());
        assertEquals(2, list.size());
        assertFalse(list.isEmpty());
        list.pop();
        assertEquals("Stack{ head={data=aa, sig->null}, count=1}",list.toString());
        assertEquals("aa", list.peek());
        assertEquals(1, list.size());
        assertFalse(list.isEmpty());
        list.pop();
        assertEquals("Stack{ head=null, count=0}",list.toString());
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
    }
}
