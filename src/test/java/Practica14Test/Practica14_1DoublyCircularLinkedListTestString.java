package Practica14Test;

import Practica14.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Practica14_1DoublyCircularLinkedListTestString {

    @Test
    public void testSizeString(){
        List<String> list = new DoublyCircularLinkedList<>();
        assertEquals(0, list.size());

        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");

        assertEquals(5, list.size());

        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        list.add("15");
        list.add("16");

        assertEquals(12, list.size());
    }

    @Test
    public void testIsEmptyString(){
        List<String> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());

        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");

        assertFalse(list.isEmpty());
    }

    @Test
    public void testGetString(){
        List<String> list = new DoublyCircularLinkedList<>();

        list.add("3");
        list.add("8");
        list.add("5");
        list.add("9");
        list.add("2");

        Object number1 = "2";
        Object number2 = "8";
        Object number3 = "9";

        assertEquals(number1, list.get(4));
        assertEquals(number2, list.get(1));
        assertEquals(number3, list.get(3));

    }

    @Test
    public void testReverseString() {
        List<String> list = new DoublyCircularLinkedList<>();
        List<String> list2 = new DoublyCircularLinkedList<>();
        List<String> list3 = new DoublyCircularLinkedList<>();
        List<String> list4 = new DoublyCircularLinkedList<>();
        List<String> list5 = new DoublyCircularLinkedList<>();
        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        assertArrayEquals(new String[]{"10", "11", "12", "13", "14"}, list.toArray());
        list.reverse();
        assertArrayEquals(new String[]{"14", "13", "12", "11", "10"}, list.toArray());

        list2.add("A");
        list2.add("B");
        list2.add("C");
        list2.add("D");
        list2.add("E");
        list2.add("F");
        list2.add("G");
        assertArrayEquals(new String[]{"A", "B", "C", "D", "E", "F", "G"}, list2.toArray());
        list2.reverse();
        assertArrayEquals(new String[]{"G", "F", "E", "D", "C", "B", "A"}, list2.toArray());

        list3.add("Maria");
        list3.add("Laura");
        list3.add("Carla");
        list3.add("Marco");
        list3.add("Matias");
        assertArrayEquals(new String[]{"Maria", "Laura", "Carla", "Marco", "Matias"}, list3.toArray());
        list3.reverse();
        assertArrayEquals(new String[]{"Matias", "Marco", "Carla", "Laura", "Maria"}, list3.toArray());

        list4.add("Pedro");
        assertArrayEquals(new String[]{"Pedro"}, list4.toArray());
        list4.reverse();
        assertArrayEquals(new String[]{"Pedro"}, list4.toArray());

        list5.add("Sergio");
        list5.add("1");
        assertArrayEquals(new String[]{"Sergio", "1"}, list5.toArray());
        list5.reverse();
        assertArrayEquals(new String[]{"1", "Sergio"}, list5.toArray());
    }
}
