package TareaEnClases;

public class Node<T> {
    //Declaracion de atributos
    private int dato;
    private Node<T> next;

    //Constructor
    public Node(int dato) {
        this.dato = dato;
    }

    //Metodos getter and setters
    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    //Metodo toString
    public String toString() {
        String string = " " + dato + " ";
        return string;
    }
}
