package Practica13;
public interface List<T> extends Comparable<T> {

    int size();

    boolean isEmpty();

    T get(int index);

    void mergeSort();

}
