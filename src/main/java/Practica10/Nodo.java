package Practica10;
class Nodo <T>{

    public T dato;
    public Nodo<T> sig;

    public Nodo(T dato) {
        this.dato = dato;
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }

    public Nodo<T> getSig() {
        return sig;
    }

    public void setSig(Nodo<T> sig) {
        this.sig = sig;
    }

    public String toString() {
        return "{dato=" + dato + ", sig->" + sig + "}";
    }
}