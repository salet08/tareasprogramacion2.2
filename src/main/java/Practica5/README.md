# SaletTareasProgramacionII.1



## Autor: Salet Yasmin Gutierrez Nava
En la presente tarea se resolvio la tarea del `Rotator` de una manera que 
el Array gire ya sea a la izquierda o a la derecha, depende del numero de
veces que se requeria, ya sea positivo o negativo, al final retornaria la lista 
la lista ya girada. De acuardo a las instrucciones que nos dio en el programa 
de `Codewars` que es lo siguiente:

## Instruccion de la Practica de Rotator

Cree un método llamado "rotar" que devuelva una matriz dada con los 
elementos dentro de los `n` espacios rotados de la matriz.

Si n es mayor que 0, debe rotar la matriz hacia la derecha. 
Si n es menor que 0, debe rotar la matriz hacia la izquierda. Si n es 0, 
entonces debería devolver la matriz sin cambios.

Ejemplo:

```
with array [1, 2, 3, 4, 5]

n = 1      =>    [5, 1, 2, 3, 4]
n = 2      =>    [4, 5, 1, 2, 3]
n = 3      =>    [3, 4, 5, 1, 2]
n = 4      =>    [2, 3, 4, 5, 1]
n = 5      =>    [1, 2, 3, 4, 5]
n = 0      =>    [1, 2, 3, 4, 5]
n = -1     =>    [2, 3, 4, 5, 1]
n = -2     =>    [3, 4, 5, 1, 2]
n = -3     =>    [4, 5, 1, 2, 3]
n = -4     =>    [5, 1, 2, 3, 4]
n = -5     =>    [1, 2, 3, 4, 5]
```

La rotación no debe estar limitada por los índices disponibles en la matriz. Lo que significa que si excedemos los índices de la matriz, sigue girando.

Ejemplo:

```
with array [1, 2, 3, 4, 5]

n = 7        =>    [4, 5, 1, 2, 3]
n = 11       =>    [5, 1, 2, 3, 4]
n = 12478    =>    [3, 4, 5, 1, 2]
```

Con sus respectivos test que son los siguientes:

```
import static org.junit.Assert.*;
import org.junit.*;

public class Practica5Test.Practica5TestRotator {
	private Rotator rotator;
  
  @Before
  public void setUp() {
  	this.rotator = new Rotator();
  }
  
  @After
  public void tearDown() {
  	this.rotator = null;
  }
  
  @Test
  public void testRotateOne_ArrayOfFive() {
  	assertArrayEquals(new Object[]{5, 1, 2, 3, 4},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, 1));
  }
 
 	@Test
  public void testRotateTwo_ArrayOfFive() {
  	assertArrayEquals(new Object[]{4, 5, 1, 2, 3},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, 2));
  }
  
  @Test
  public void testRotateThree_ArrayOfFive() {
  	assertArrayEquals(new Object[]{3, 4, 5, 1, 2},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, 3));
  }
  
  @Test
  public void testRotateFour_ArrayOfFive() {
  	assertArrayEquals(new Object[]{2, 3, 4, 5, 1},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, 4));
  }
 	
  @Test
  public void testRotateFive_ArrayOfFive() {
  	assertArrayEquals(new Object[]{1, 2, 3, 4, 5},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, 5));
  }
  
  @Test
  public void testRotateSix_ArrayOfFive() {
  	assertArrayEquals(new Object[]{5, 1, 2, 3, 4},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, 6));
  }
  
  @Test
  public void testRotateNegOne_ArrayOfFive() {
  	assertArrayEquals(new Object[]{2, 3, 4, 5, 1},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, -1));
  }
  
  @Test
  public void testRotateNegTwo_ArrayOfFive() {
  	assertArrayEquals(new Object[]{3, 4, 5, 1, 2},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, -2));
  }
  
  @Test
  public void testRotateNegThree_ArrayOfFive() {
  	assertArrayEquals(new Object[]{4, 5, 1, 2, 3},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, -3));
  }
  
  @Test
  public void testRotateNegFour_ArrayOfFive() {
  	assertArrayEquals(new Object[]{5, 1, 2, 3, 4},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, -4));
  }
  
  @Test
  public void testRotateNegFive_ArrayOfFive() {
  	assertArrayEquals(new Object[]{1, 2, 3, 4, 5},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, -5));
  }
  
  @Test
  public void testRotateNegSix_ArrayOfFive() {
  	assertArrayEquals(new Object[]{2, 3, 4, 5, 1},
    	rotator.rotate(new Object[]{1, 2, 3, 4, 5}, -6));
  }
}
```

Con un ejemplo de clase que es la siguiente:

```
public class Rotator {

    public Object[] rotate(Object[] data, int n) {
         return data;
    }
```
Para conseguir la solucion se dio los metodos necesarios para que al final
de la solucion y que los test corran de una manera satisfactible.

Lo que me dificulto mas fue en como se rotaria hacia la izquierda o derecha
ya que entraba aveces a un bucle infinito y cargaba los test sin salir de ninguno
de ellos, sin embargo se logro solucionar satisfactoriamente.

Y por ultimo pienso que estan bien separado cada metodo ya que cada uno
hace una tarea independientemente, sin que uno dependa del otro.

## Links

Link de la tarea a realizar: 
[Rotate Array](https://www.codewars.com/kata/5469e0798a3502f4a90005c9)