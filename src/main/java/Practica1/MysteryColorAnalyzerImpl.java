package Practica1;
import java.util.List;

public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer{

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        return (int)mysteryColors.stream().distinct().count();
    }


    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int colorCounter = 0;
        for (Color colors : mysteryColors) {
            if (colors.equals(color)) colorCounter++;
        }
        return colorCounter;
    }

}
