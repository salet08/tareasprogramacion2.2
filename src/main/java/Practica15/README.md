# Tarea N°15 Programación
## Nombre: Salet Yasmin Gutierrez Nava

**1.- Sacar screenshot de la salida del metodo**

>![PrimeraOrden](Fotos/PrimeraOrden.png)

**2.- Sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour**

+ **LlamadasEncadenadas.doFour();**
>![SegundaOrdendoFour](Fotos/SegundaOrdenDoFour.png)

`SALIDA`
>![SegundaOrdendoFourSalida](Fotos/SegundaOrdenDoFourSalida.png)

+ **LlamadasEncadenadas.doOne();**
>![SegundaOrdendoOne](Fotos/SegundaOrdenDoOne.png)

`SALIDA`
>![SegundaOrdendoOneSalida](Fotos/SegundaOrdenDoOneSalida.png)

Haciendo el Debug se va al metodo que en este caso lo hice de las dos maneras
que primero puede ser del doFour() o del doOne() donde ahi implime algunos mensajes
y llama a los metodos y asi sucesivamente, hasta que llega al ultimo e imprime lo
que falta del metodo anterior.

###Descomentar la llamda a: Infinito.whileTrue()

>![DescomentarOrdenInfinito](Fotos/DescomentarOrdenInfinito.png)

**1.- sacar screenshot de la exception**
>![PrimeraOrdenInfinito1](Fotos/PrimeraOrdenInfinito1.png)

>![PrimeraOrdenInfinito2](Fotos/PrimeraOrdenInfinito2.png)

**2.- a continuacion describir el problema:**
`Descripcion:.................`

Lo que esta pasando es que se llama al metodo de whileTrue(); 
y en el metodo se ve que se llama a si mismo y esto lo hace 
muchas veces y da un ciclo infinito y provoca un error o exepcion
que es el StackOverFlowError que dice: `Thrown when a stack overflow
occurs because an application recurses too deeply.`(Se lanza cuando
se produce un desbordamiento de pila porque una aplicación recurre
demasiado). Y si es que ya no queremos que ocurra varias veces se debe
de poner una condicion y depende a lo que queremos obtener se dara un
buen codigo.

**3.- volver a comentar la llamanda a: Infinito.whileTrue()**

>![TerceraOrdenInfinito](Fotos/TerceraOrdenInfinito.png)

###Descomentar la llamada a: Contador.contarHasta(), pasando un número entero positivo

>![DescomentarOrdenContarHasta](Fotos/DescomentarOrdenContarHasta.png)

**1.- Sacar screenshot de la salida del metodo** 

>![PrimeraOrdenSalidaMetodo](Fotos/PrimeraOrdenSalidaMetodo.png)

**2.- A continuacion describir como funciona este metodo:**
`Descripcion:.................`

``` js
    Contador.contarHasta(5);
```
En la clase Main se llama al metodo contarHasta pasandole un parametro
donde será leido y hara las operaciones que se tiene dentro del metodo
que se ve a continuacion:
``` js
    public static void contarHasta(int contador) { // for i = contador
        System.out.println(contador);
        if (contador > 0) { //while i > 0
            //contarHasta(contador - 1); // i--
            contarHasta(--contador); // i--
        }
    }
```
Entonces lo que hace primero es imprimir el contador que en este caso
seria el `5` y despues va a la condición que si el el "contador(5)" `5 > 0`
entonces que lo reste el contador llamandolo otra vez al metodo, haciendolo 
graficamente seria:

`PRIMERA VUELTA`
+ Imprimer 5
+ Condicion 5 > 0 -> true
+ Se resta el contador llamando al metodo otra vez
+ 5 - 1 = 4

`SEGUNDA VUELTA`
+ Imprimer 4
+ Condicion 4 > 0 -> true
+ Se resta el contador llamando al metodo otra vez
+ 4 - 1 = 3

`TERCERA VUELTA`
+ Imprimer 3
+ Condicion 3 > 0 -> true
+ Se resta el contador llamando al metodo otra vez
+ 3 - 1 = 2

`CUARTA VUELTA`
+ Imprimer 2
+ Condicion 2 > 0 -> true
+ Se resta el contador llamando al metodo otra vez
+ 2 - 1 = 1

`QUINTA VUELTA`
+ Imprimer 1
+ Condicion 1 > 0 -> true
+ Se resta el contador llamando al metodo otra vez
+ 1 - 1 = 0

`SEXTA VUELTA`
+ Imprimer 0
+ Condicion 0 > 0 -> false
+ Sale del metodo

`SALIDA`

5 

4 

3

2

1

0
