package Practica17;

public class Stack <T extends Comparable<T>>{
    private Node<T> head;
    private int count;

    public Stack() {
        this.head = null;
        this.count = 0;
    }

    public void push(T nodeData) {
        Node<T> node = new Node<>(nodeData);
        node.setNext(head);
        head = node;
        count ++;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public T peek() {
        if (isEmpty()) {
            System.exit(1);
        }
        return head.getData();
    }

    public T pop(){
        if (isEmpty()) {
            System.exit(-1);
        }
        T higher = peek();
        count --;
        head = head.getNext();
        return higher;
    }

    public int size() {
        return count;
    }

    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }

    @Override
    public String toString() {
        return head +
                ", count=" + count;
    }
}
