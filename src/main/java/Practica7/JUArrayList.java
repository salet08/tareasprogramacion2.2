package Practica7;
import java.util.*;

public class JUArrayList implements List<Integer>{

    private Integer[] arr = new Integer[0];
    private int index = 0;

    public void setArr(Integer[] arr) {
        this.arr = arr;
    }

    @Override
    public int size() {
        return arr.length;
    }

    @Override
    public boolean isEmpty() {
        return arr.length > 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        Iterator<Integer> it = new Iterator<Integer>() {
            private int currentIndex = 0;
            @Override
            public boolean hasNext() {
                return currentIndex < arr.length && arr[currentIndex] != null;
            }
            @Override
            public Integer next() {
                return arr[currentIndex++];
            }
        };
        return it;
    }

    @Override
    public boolean add(Integer integer) {
        if (index == size()){
            Integer[] arrayAux = new Integer[size()+1];
            for (int i = 0; i < size(); i++) {
                arrayAux[i]=arr[i];
            }
            arr = arrayAux;
            arr[index]=integer;
            index++;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {

        Integer[] arrayAux= new Integer[size()-1];

        for (int i = 0, j = 0 ; i < arr.length; i++) {
            if (!arr[i].equals(o)){
                arrayAux[j]=arr[i];
            }else {
                j--;
            }
            j++;
        }
        arr = arrayAux;
        return true;
    }

    @Override
    public void clear() {
        Arrays.fill(arr, 0);
    }

    public Integer[] getArr() {
        return arr;
    }

    @Override
    public Integer get(int index) {
        return arr[index];
    }

    @Override
    public Integer remove(int index) {
        Integer integer = arr[index];
        arr[index]=null;
        return integer;
    }
    /**
     * no se pudo relizar el metodo por que me daba error
     * no se si sera problema de el intellij o que no se
     * encontro el metodo, lo puse en comentarios si es que
     * le sale el error de la misma manera.
     */

    /*@Override
    public <T> T[] toArray(T[] a) {

    }*/

    //Opcionales

    @Override
    public boolean contains(Object object) {
        boolean itIsTrueOrFalse = false;
        for (Integer integer:arr) {
            if (integer == object){
                itIsTrueOrFalse = true;
                break;
            }
        }
        return itIsTrueOrFalse;
    }

    @Override
    public Integer set(int index, Integer element) {
        return null;
    }

    @Override
    public void add(int index, Integer element) {

    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }



//No hacer
    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        return false;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator listIterator() {
        return null;
    }

    @Override
    public ListIterator listIterator(int index) {
        return null;
    }

    @Override
    public List subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

}
