package Practica14;
public class DoublyCircularLinkedList<T> implements List<T> {

    private Node<T> head;
    private Node<T> previous;

    public DoublyCircularLinkedList() {
    }

    @Override
    public int size() {
        Node<T> nodeHead = head;
        int countLength = 0;
        if (head != null) {
            do {
                nodeHead = nodeHead.getNext();
                countLength++;
            } while (nodeHead != head);
        }
        return countLength;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T get(int index) {
        Node<T> node = head;
        if (isEmpty() || index < 0) return null;
        else {
            for (int i = 0; i < index; i++) {
                node = node.getNext();
            }
        }
        return node.getData();

    }

    public boolean add(T data) {
        Node<T> newNodo = new Node<>(data);
        if (head == null) {
            head = newNodo;
            previous = newNodo;
            head.setNext(head);
            previous.setPrevious(previous);
            return true;
        } else {
            previous.setNext(newNodo);
            newNodo.setNext(head);
            newNodo.setPrevious(previous);
            previous = newNodo;
            head.setPrevious(previous);
            return true;
        }
    }

    @Override
    public void reverse() {
        Node<T> previous = null, current = head, next;
        do {
            next = current.getNext();
            current.setNext(previous);
            previous = current;
            current = next;
        } while (current != head);
        head.setNext(previous);
        head = previous;
    }

    private Node<T> getNode(int index){
        Node<T> node = null;
        if (index >= 0 && index < size()){
            node = head;
            for (int i = 0; i < index; i++){
                node = node.getNext();
            }
        }
        return  node;
    }

    @Override
    public Object[] toArray() {
        Object[] toArray = new Object[size()];
        for (int i=0;i<size() ;i++){
            toArray[i] = getNode (i).getData();
        }
        return toArray;
    }

    @Override
    public int compareTo(T o) {
        return 0;
    }

}
