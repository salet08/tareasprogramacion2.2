package Practica12Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Practica12TestCircularLinkedList {
    /*@Test
    void add(){
        List<Integer> list = new CircularLinkedList<>();
        assertTrue(list.isEmpty());
        list.add(1);
        //1,
        assertFalse(list.isEmpty());
        assertEquals(1, list.size());
        list.add(2);
        //1,2,
        assertEquals(2, list.size());
        assertEquals(1, list.get(0));
        assertEquals(2, list.get(1));
        list.add(3);
        //1,2,3,
        assertEquals(3, list.size());
        assertEquals(1, list.get(3));
        list.add(4);
        //1,2,3,4,
        assertEquals(4, list.size());
        assertEquals(4, list.get(3));
        assertEquals(1, list.get(4));
        list.add(5);
        //1,2,3,4,5,
        assertEquals(5, list.size());
        assertEquals(1, list.get(0));
        assertEquals(5, list.get(4));
        assertEquals(1, list.get(5));
        System.out.println(list);
    }

    @Test
    void remove() {
        List<Integer> list = CircularLinkedList.dummyList(5);
        //5,4,3,2,1
        assertEquals(false,list.isEmpty());
        assertEquals(5, list.size());
        assertEquals(5, list.get(0));
        assertEquals(5, list.get(5));
        assertEquals(true, list.remove(5));
        //assertTrue(list.remove(5));
        //4,3,2,1
        assertEquals(4, list.size());
        assertEquals(4, list.get(0));
        assertEquals(4, list.get(4));
        //4,3,2
        assertEquals(true, list.remove(1));
        assertEquals(3, list.size());
        assertEquals(4, list.get(0));
        assertEquals(3, list.get(4));
        assertEquals(true, list.remove(3));
        //4,2
        assertEquals(2, list.size());
        assertEquals(4, list.get(0));
        assertEquals(2, list.get(3));
        assertEquals(false, list.remove(3));
        //4,2
        assertEquals(2, list.size());
        assertEquals(4, list.get(0));
        assertEquals(2, list.get(3));
        assertEquals(true,list.remove(2));
        //4
        assertEquals(4, list.get(0));
        assertEquals(4, list.get(3));
    }*/
}